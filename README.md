# UVvisToolBox

## Installation

Given that **UVvisToolBox.jl** is not a registered julia package, for installation the *url* to the repository and the julia package manager are necessary.

```julia
using Pkg
Pkg.add(PackageSpec(url="https://bitbucket.org/Denice_van_Herwerden/uvvistoolbox/src/master/"))


```
