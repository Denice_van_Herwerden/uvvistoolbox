import numpy as np
import bson
import os
from pathlib import Path

from lmfit.models import GaussianModel

from pymcr.mcr import McrAR
from pymcr.constraints import Constraint, ConstraintNorm

class ConstSingleGauss(Constraint):
    # """
    # Perform a nonlinear least-squares fitting to enforce a Gaussian.
    #
    # Parameters
    # ----------
    # copy : bool
    #     Make copy of input data, A; otherwise, overwrite (if mutable)
    #
    # axis : int
    #     Axis to perform fitting over
    #
    # """
    def __init__(self, copy=False, axis=-1):
        # """ A must be non-negative"""
        self.copy = copy
        self.axis = axis

    def transform(self, A):
        # """ Fit """
        global test
        n_components = list(A.shape)
        x = np.arange(n_components[self.axis])
        n_components.pop(self.axis)
        assert len(n_components)==1, 'Input must be 2D'
        n_components = n_components[0]

        A_fit = 0*A

        #load file with input parameters from julia
        source_path = Path(__file__).resolve()
        print(source_path.parent)
        print(os.path.join(source_path.parent,'/aux/temp.bson'))
        data = bson.loads(open(source_path.parent+'/aux/temp.bson', 'rb'))
        # b = bson.loads()

        for num in range(n_components):
            if (self.axis == -1) | (self.axis == 1):
                y = A[num, :]
            else:
                y = A[:, num]

            mod = GaussianModel()
            pars = mod.guess(y, x=x)
# Parameters([('amplitude', <Parameter 'amplitude', value=0.8485722256044884, bounds=[-inf:inf]>),
#             ('center', <Parameter 'center', value=2, bounds=[-inf:inf]>),
#             ('sigma', <Parameter 'sigma', value=0.3333333333333333, bounds=[0.0:inf]>),
#             ('fwhm', <Parameter 'fwhm', value=0.78494, bounds=[-inf:inf], expr='2.3548200*sigma'>),
#             ('height', <Parameter 'height', value=1.0155940661963208, bounds=[-inf:inf],
#             expr='0.3989423*amplitude/max(1e-15, sigma)'>)])
            #adjust pars according to info
            #amplitude bounds 0:Inf
            # ...
            out = mod.fit(y, pars, x=x)

            if (self.axis == -1) | (self.axis == 1):
                A_fit[num,:] = 1*out.best_fit
            else:
                A_fit[:, num] = 1*out.best_fit

        if self.copy:
            return A_fit
        else:
            A *= 0
            A += A_fit
            return A



class ConstraintNorm21(Constraint):
    """
    Normalization constraint.
    Parameters
    ----------
    axis : int
        Which axis of input matrix A to apply normalization acorss.
    fix : list
        Keep fix-axes as-is and normalize the remaining axes based on the
        residual of the fixed axes.
    set_zeros_to_feature : int
        Set all samples which sum-to-zero across axis to 1 for a particular
         feature (See Notes)
    copy : bool
        Make copy of input data, A; otherwise, overwrite (if mutable)
    Notes
    -----
    -   For set_zeros_to_feature, assuming the data represents concentration
         with a matrix [n_samples, n_features] and the axis is across the
         features, for every sample that sums to 0 across axis, would be
         replaced with a vector [n_features] of zeros except at
         set_zeros_to_feature, which would equal 1. I.e., this pixel is
         now pure substance of index value set_zeros_to_feature.
    """
    def __init__(self, axis=-1, fix=None, copy=False):
        """Normalize along axis"""
        super().__init__(copy)
        if fix is None:
            self.fix = fix
        elif isinstance(fix, int):
            self.fix = [fix]
        elif isinstance(fix, (list, tuple)):
            self.fix = fix
        elif isinstance(fix, np.ndarray):
            if np.issubdtype(fix.dtype, np.integer):
                self.fix = fix.tolist()
            else:
                raise TypeError('ndarrays must be of dtype int')
        else:
            raise TypeError('Parameter fix must be of type None, int, list,',
                            'tuple, ndarray')

        if not ((axis == 0) | (axis == 1) | (axis == -1)):
            raise ValueError('Axis must be 0,1, or -1')
        self.axis = axis

    def transform(self, A):
        """ Apply normalization constraint """

        if self.copy:
            if self.axis == 0:
                if not self.fix:  # No fixed axes
                    return A / A.max(axis=self.axis)[None, :]
                else:  # Fixed axes
                    fix_locs = self.fix
                    not_fix_locs = [v for v in np.arange(A.shape[0]).tolist()
                                    if self.fix.count(v) == 0]
                    scaler = np.ones(A.shape)
                    div = A[not_fix_locs, :].sum(axis=0)[None, :]
                    div[div == 0] = 1
                    scaler[not_fix_locs, :] = ((1 - A[fix_locs, :].sum(axis=0)[None, :]) / div)

                    return A * scaler
            else:  # Axis = 1 / -1
                if not self.fix:  # No fixed axes
                    return A / A.max(axis=self.axis)[:, None]
                else:  # Fixed axis
                    fix_locs = self.fix
                    not_fix_locs = [v for v in np.arange(A.shape[-1]).tolist()
                                    if self.fix.count(v) == 0]
                    scaler = np.ones(A.shape)
                    div = A[:, not_fix_locs].sum(axis=-1)[:, None]
                    div[div == 0] = 1
                    scaler[:, not_fix_locs] = ((1 - A[:, fix_locs].sum(axis=-1)[:, None]) / div)

                    return A * scaler
        else:  # Overwrite original data
            if A.dtype != np.float:
                raise TypeError('A.dtype must be float for',
                                'in-place math (copy=False)')

            if self.axis == 0:
                if not self.fix:  # No fixed axes
                    A /= A.max(axis=self.axis)[None, :]
                    return A
                else:  # Fixed axes
                    fix_locs = self.fix
                    not_fix_locs = [v for v in np.arange(A.shape[0]).tolist()
                                    if self.fix.count(v) == 0]
                    scaler = np.ones(A.shape)
                    div = A[not_fix_locs, :].sum(axis=0)[None, :]
                    div[div == 0] = 1
                    scaler[not_fix_locs, :] = ((1 - A[fix_locs, :].sum(axis=0)[None, :]) / div)
                    A *= scaler
                    return A
            else:  # Axis = 1 / -1
                if not self.fix:  # No fixed axes
                    A /= A.max(axis=self.axis)[:, None]
                    return A
                else:  # Fixed axis
                    fix_locs = self.fix
                    not_fix_locs = [v for v in np.arange(A.shape[-1]).tolist()
                                    if self.fix.count(v) == 0]
                    scaler = np.ones(A.shape)
                    div = A[:, not_fix_locs].sum(axis=-1)[:, None]
                    div[div == 0] = 1
                    scaler[:, not_fix_locs] = ((1 - A[:, fix_locs].sum(axis=-1)[:, None]) / div)
                    A *= scaler
                    return A
