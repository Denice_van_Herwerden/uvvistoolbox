using CSV, DataFrames, Statistics, LinearAlgebra, SparseArrays
include("DAD_Import.jl")

using PyCall
using Conda
using BSON
mcr = pyimport("pymcr.mcr")
con = pyimport("pymcr.constraints")
pushfirst!(PyVector(pyimport("sys")."path"), @__DIR__)
c = pyimport("ConstSingleGauss")





function DADdeconvolution(filename,path,type; kwargs...)
	#kwargs... :filename_bg, :path_bg

	#unpacking of kwargs
	while any(keys(kwargs) .== :kwargs)
		kwargs = kwargs[:kwargs]
	end

	#laod peak list
	if isfile(path*filename[1:findlast('.',filename)-1]*"_peak list.csv")
		peak_list = CSV.read(path*filename[1:findlast('.',filename)-1]*"_peak list.csv",DataFrame)
	else
		println(path*filename[1:findlast('.',filename)-1]*"_peak list.csv is not a valid peak list file")
		return
	end

	#load chromatogram
	println(filename)
	println(path)
	trw, signal, lambda = importDAD(filename,path)

	#background correction
	if any(keys(kwargs) .== :filename_bg)
		if any(keys(kwargs) .== :path_bg)
			tr_bg, signal_bg = importDAD(kwargs[:filename_bg], kwargs[:path_bg])
		else
			tr_bg, signal_bg = importDAD(kwargs[:filename_bg], path)
		end
		#check if substraction is possible
		signal = signal .- signal_bg
	end

	#Perform extraction
	if type == "direct"
		peak_list, spec = DirectExtraction(peak_list,trw,signal,lambda)
	elseif type == "MCR-ALS"
		peak_list, spec = Deconvolution(peak_list, trw, signal, lambda, type)
	else
		println("The peak detection method is unknown. Please choose one of the following options:
				 1) \"Direct\": direct extraction of the spectra
				 2) \"MCRALS\": deconvolution using MCR-ALS")
	end

	#Add information to peak list
	peak_list = addSpectra(peak_list, spec, lambda)

	#Save the peak list
	CSV.write(path*filename[1:findlast('.',filename)-1]*"_peak list.csv",peak_list)

	return spec, peak_list
end


function DADdetdeconvolution(peak_list,trw,signal,lambda,type; kwargs...)

	#unpacking of kwargs
	while any(keys(kwargs) .== :kwargs)
		kwargs = kwargs[:kwargs]
	end

	#Perform extraction
	if type == "direct"
		peak_list, spec = DirectExtraction(peak_list,trw,signal,lambda)
	elseif type == "MCR-ALS"
		peak_list, spec = Deconvolution(peak_list, trw, signal, lambda, type)
	else
		println("The peak detection method is unknown. Please choose one of the following options:
				 1) \"Direct\": direct extraction of the spectra
				 ))")#2) \"MCRALS\": deconvolution using MCR-ALS"))
	end

	#Add information to peak list
	peak_list = addSpectra(peak_list, spec, lambda)

	return peak_list
end



function DADdeconvolutionBatch(path,type; kwargs...)
	files = readdir(path)
	for f = 1:length(files)
		if isfile(path*files[f])
			if cmp(files[f][end-12:end],"peak list.csv") != 0
				if isempty(kwargs.data)
					println("Deconvoluting: "*files[f])
					DADdeconvolution(files[f],path,type)
					println(files[f]*" Finished")
				elseif typeof(kwargs[:filename_bg]) == String
					println("Deconvoluting: "*files[f]*" with blank removal")
					DADdeconvolution(files[f],path,type; kwargs)
					println(files[f]*" Finished")
				end
			end
		end
	end
	println("Batch processing has finished")
end



## Spectral extraction functions
function DirectExtraction(peak_list, trw, signal, lambda)
	#Directly extract the spectra from the chromatogram at the corresponding retention times

	#Generate matrix for saving spectra
	spec = zeros(size(peak_list,1),length(lambda))

	#Extract spectrum for each peak
	for i = 1:size(peak_list,1)
		ind = findmin(abs.(trw .- peak_list[!,"t_avg"][i]))[2]
		spec[i,:] = signal[ind,:]
	end

	return peak_list, spec
end




function Deconvolution(peak_list, trw, signal, lambda, type)

	#sort on tr
	peak_list = peak_list[sortperm(peak_list[!,"t_avg"]),:]
	statDeconv = Int.(zeros(size(peak_list,1)))
	spec_all = zeros(size(peak_list,1),size(signal,2))
	conc_all = zeros(length(trw),size(peak_list,1))

	#calc w05 for initial guess
	# w05 =
	#initialize MCR function
	mcrar = mcr.McrAR(max_iter=50, st_regr="NNLS", c_regr="OLS",
	              c_constraints=[c.ConstSingleGauss()],
				  st_constraints=[c.ConstraintNorm21()])

	# peak_list = peak_list[1:5,:]
	# ind = 2
	# ind, st, ed = overlapcheck(ind, trw, signal, peak_list)
	#
	# mcrar.fit(D=signal[st:ed,:], ST=signal[peak_list[!,"tStart"][ind],:])
	# mcrar.fit(D=signal[st:ed,:], C=signal[st:ed,vec([1 205 453])])
	#
	# using Plots
	# plot(mcrar.C_opt_)
	# plot(mcrar.ST_opt_')




	#find bracketing region
	while any(statDeconv .== 0)
		#low to high tr
		ind = findfirst(statDeconv .== 0)
		ind, st, ed = overlapcheck(ind, trw, signal, peak_list)

		#sort selected peaks on intensity
		pks_sel = peak_list[ind,:]
		pks_sel = pks_sel[sortperm(pks_sel[!,"maxInt"]),:]

		#save info for python script
		trs = findfirst.(pks_sel[!,"t_avg"] .< trw')
		bson("C:\\Users\\dherwer\\.julia\\dev\\UVvisToolBox\\src\\aux\\temp.bson",
			 	Dict(:tr => pks_sel[!,"t_avg"][ind], :dtrmax => 0.025))
		#Initial concentrations
		conc = zeros(length(trw[st:ed]),length(ind))
		for c = 1:size(conc,2)
			# conc[:,c] = ...
		end

		#perform deconvolution
		mcrar.fit(D=signal[st:ed,:], C=signal[st:ed,400:402])

		#Save results to spec
		for i = 1:length(ind)
			conc_all[:,ind[i]] = conc[:,i]
			spec_all[ind[i],:] = spec[i,:]
		end

		#set finished spectra to 0
		statDeconv[ind] .= 1
	end
	return peak_lsit, spec_all
end



## General functions
function addSpectra(peak_list, spec, lambda)
	#Function that adds the spectra, min and max lambda, and stepsize to the peak list

	#expand dataframe columns
	peak_list[!,"Spectrum"] = Array{Union{Missing,String}}(missing,size(peak_list,1))
	peak_list[!,"Min_wavelength"] = ones(size(peak_list,1)).*lambda[1]
	peak_list[!,"Max_wavelength"] = ones(size(peak_list,1)).*lambda[end]
	peak_list[!,"Step_size"] = ones(size(peak_list,1)).*(lambda[2] - lambda[1])

	#add information
	for i = 1:size(peak_list,1)
		peak_list[!,"Spectrum"][i] = string(spec[i,:])
	end

	return peak_list
end




function overlapcheck(ind, trw, signal, peak_list)
	#Checks overlap of peak of interest with neighbours
	#Determine start and end of peak
	TIC = sum(signal, dims=2)
	z = AsLS(TIC, 100000, 0.001)

	pk_loc = findfirst(trw .> peak_list[!,"t_avg"][ind])

	st = findlast(TIC[1:pk_loc] .< z[1:pk_loc])
	ed = findfirst(TIC[pk_loc:end] .< z[pk_loc:end]) + pk_loc - 1

	ind = findall(trw[st] .< peak_list[!,"t_avg"] .< trw[ed])

	return ind, st, ed
end





function AsLS(y, lambda, p)
	y = vec(y)

	# Estimate baseline with asymmetric least squares
	m = length(y)
	d = zeros(m,m)
	d[diagind(d)] .= 1
	D = diff(diff(d, dims = 1),dims =1)
	w = ones(m, 1)
	z = []

	for it = 1:10
		W = spdiagm(m, m, 0 => vec(w))
	    C = cholesky(W + lambda * D' * D)
	    z = C.U \ (C.U' \ (w .* y))
	    w = p * (y .> z) + (1 - p) * (y .< z)
	end

	return z
end





## testing section

# include("DAD_Import.jl")
#
# filename = "026-P2-B5-19.CSV"
# path = "C:\\Users\\dherwer\\OneDrive - UvA\\UV-Vis\\Datafiles\\Deconvolution\\"
#
# if isfile(path*filename[1:findlast('.',filename)-1]*"_peak list.csv")
# 	peak_list = CSV.read(path*filename[1:findlast('.',filename)-1]*"_peak list.csv",DataFrame)
# else
# 	println(path*filename[1:findlast('.',filename)-1]*"_peak list.csv is not a valid peak list file")
# 	return
# end
# peak_list = peak_list[1:5,:]
#
# #load chromatogram
# trw, signal, lambda = importDAD(filename,path)


#
# ###with bg correction
# spec,pkl = DADdeconvolution(filename,path,"direct";filename_bg = "029-P2-B11-Blank.CSV")
# DADdeconvolutionBatch(path,"direct";filename_bg = "029-P2-B11-Blank.CSV")
#
# ###without bg correction
# DADdeconvolution(filename,path,"direct")
# DADdeconvolutionBatch(path,"direct")

#
#
# #5 min blank = 004
# #20 min blank = 029
# using Statistics
# corr = zeros(size(spec,1),size(spec,1))
# for i = 1:size(spec,1)
# 	for j = 1:size(spec,1)
# 		corr[i,j] = cor(spec[i,:],spec[j,:])
# 	end
# end
#
#
#
#
# ## Tijmen
# filename = "020-P1-E1-Dye 1-25 MegaMix.CSV"
# path = "C:\\Users\\dherwer\\OneDrive - UvA\\UV-Vis\\Datafiles\\Tijmen\\20 min"
#
# spec,peak = DADdeconvolution(filename,path,"direct";filename_bg = "019-NV-Blank.CSV")
