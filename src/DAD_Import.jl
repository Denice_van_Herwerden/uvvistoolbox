using CSV, DataFrames, DelimitedFiles, StringEncodings

function importDAD(filename,path)
	#Own file when multiple methods are available
	temp = readlines(path*filename, enc"UTF-16");
	lambda = parse.(Float64,split(temp[1][2:end],','))
	data = zeros(length(temp)-1,length(lambda)+1)
	for r = 2:length(temp)
		data[r-1,:] = parse.(Float64,split(temp[r],','))'
	end
	tr = data[:,1]
	signal = data[:,2:end]
	return tr, signal, lambda
end






## testing section
