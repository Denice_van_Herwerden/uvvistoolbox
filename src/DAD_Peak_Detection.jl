using Findpeaks, DataFrames, Statistics, LinearAlgebra, SparseArrays
include("DAD_Import.jl")
include("DAD_Deconvolution.jl")
#temp
using Plots




function DADPeakDetection(filename,path, minInt, deltaTime, minWV, type; kwargs...)
	#kwargs: filename_bg, path_bg, time_window, Deconvolution

	# unpacking kwargs
	while any(keys(kwargs) .== :kwargs)
		kwargs = kwargs[:kwargs]
	end
	println("")

	#import DAD file
	trw, signal, wv = importDAD(filename,path)
	minLambdaCount = floor(minWV/mean(diff(wv)))			#number of datapoints required to fullfill the minimum peak broadness in the wavelngth domain

	#Check orientation of the time and signal variable
	if size(trw,1) == 1
		trw = trw'
	end

	if size(trw,1) != size(signal,1)
		signal = signal'
	end


	#Check for optional background correction method
	if any(keys(kwargs) .== :filename_bg)
		if any(keys(kwargs) .== :path_bg)
			tr_bg, signal_bg = importDAD(kwargs[:filename_bg], kwargs[:path_bg])
		else
			tr_bg, signal_bg = importDAD(kwargs[:filename_bg], path)
		end
		#check if substraction is possible
		if size(signal) == size(signal_bg)
			signal = signal .- signal_bg
		else
			println("Background correction skipped due to mismatching size")
		end
	# elseif any(keys(kwargs) .== :MovingAvg)
	# 	est_bg = BackgroundEstimation(signal,"MovingAvg")
	end


	#Check time window cutting
	if any(keys(kwargs) .== :time_window)
		#check input for time window
		if any(typeof(kwargs[:time_window]) .== [Int64 Float64])
			#1 number is end of chrom
			if minimum(trw) .> kwargs[:time_window]
				println("Time window not adjusted, since first time point is larger then provided time window")
			elseif maximum(trw) .< kwargs[:time_window]
				println("Time window not adjusted, since last time point is smaller then provided time window")
			else
				maxInd = findlast(trw .< kwargs[:time_window])
				trw = trw[1:maxInd]
				signal = signal[1:maxInd,:]
				println("Time range will be adjust to "*string(minimum(trw))*" - "*string(kwargs[:time_window]))
			end
		elseif size(kwargs[:time_window]) == (1, 2) || size(kwargs[:time_window]) == (1, 2)
			#2 numbers is range
			minTime = minimum(kwargs[:time_window])
			maxTime = maximum(kwargs[:time_window])

			if minTime .> maximum(trw) || maxTime .< minimum(trw)
				println("Selected window range is out of range for the chromatogram")
			else
				minInd = findfirst(trw .> minTime)
				maxInd = findlast(trw .< maxTime)
				trw = trw[minInd:maxInd]
				signal = signal[minInd:maxInd,:]
				println("Time range will be adjust to "*string(trw[minInd])*"-"*string(trw[maxInd]))
			end
		else
			#error check input
			println("Provided input for time_window not compatible. Either provide a single number for the maximum time (time_window = 5.3) or an array containing the minimum and maximum time (time_window = [1.6 4.3])")
		end
	end


	#Perform peak detection
	if type == "MWPD"
		#Multiple wavelength peak detection
		peak_list = MultipleWavelengthPeakDetection(trw,wv,signal,minInt, deltaTime, minLambdaCount)
	elseif type == "TIC"
		peak_list = TICPeakDetection(trw, wv, signal, minInt)
	else
		println("The peak detection method is unknown. Please choose one of the following options:
				 1) \"MWPD\": Multiple wavelength peak detection")
	end

	#Filter peak list based on Flagging
	if any(keys(kwargs) .== :FlagThreshold)
		peak_list = peak_list[peak_list[!,"FlagCor"] .> kwargs[:FlagThreshold],:]
	end


	if any(keys(kwargs) .== :Deconvolution)
		println("Obtaining spectra")
		peak_list = DADdetdeconvolution(peak_list,trw,signal,wv,kwargs[:Deconvolution])
	end



	#Save peak list
	if isnothing(peak_list)
		if isfile(path*filename[1:findlast('.',filename)-1]*"_peak list.csv")
			rm(path*filename[1:findlast('.',filename)-1]*"_peak list.csv")
		end
	else
		CSV.write(path*filename[1:findlast('.',filename)-1]*"_peak list.csv", peak_list)
		###temp###
		if type == "MWPD" && all(keys(kwargs) .!= :figures)
			plotResults(peak_list, trw, signal, wv, filename, path)
		end
	end


	return peak_list
end



#Improve by runnig parallel
function DADPeakDetectionBatch(path, minInt, deltaTime, minLambdaCount, type; kwargs...)
	#process multiple files in the dame directory
	if any(keys(kwargs) .== :filenames)
		files = kwargs[:filenames]
	else
		files = readdir(path)
	end
	for f = 1:length(files)
		if isfile(path*files[f]) && (cmp(files[f][end-12:end],"peak list.csv") != 0)
			if isempty(kwargs.data)
				DADPeakDetection(files[f],path, minInt, deltaTime, minLambdaCount, type)
			elseif any(keys(kwargs).== :MovingAvg)
				println("Processing "*files[f]*" with bg estimation")
				DADPeakDetection(files[f],path, minInt, deltaTime, minLambdaCount, type; kwargs)
			elseif typeof(kwargs[:filename_bg]) == String
				#same blank for all measurements
				if cmp(files[f], kwargs[:filename_bg]) == 0
					println("Skipped background measurement "*files[f])
					continue
				end
				println("Processing "*files[f]*" with blank removal")
				DADPeakDetection(files[f],path, minInt, deltaTime, minLambdaCount, type; kwargs)
			end
		end
	end
end



## Peak detection functions

function TICPeakDetection(trw, wv, signal, percInt,filename) 	#rm filename after temp
	#currently evaluating if TIC peak detection detects more/less peaks
	if size(trw) != size(signal)
		TIC = sum(signal, dims=2)
	end
	tr_d1, TIC_d1 = deriv(trw, TIC)
	tr_d2, TIC_d2 = deriv(tr_d1, TIC_d1)

	#Detect peak maxima
	prom = minPeakPromCalc(max(TIC)*percInt,trw,TIC)
	#Find negative peaks in 2nd derivative
	peaks = zeros(size(TIC))
	peaks[findpeaks(-1*TIC_d2[:,1],min_prom=prom*size(signal,2)).+1,1] .= -1
	#Cleanup based on the minimum signal intensity & single points
	peaks_filt = ((peaks .== -1) .+ (TIC .>= minInt)) .== 2
	sum(peaks_filt)

	plot(peaks)
	plot!(TIC./maximum(TIC))

	savefig("C:\\Users\\dherwer\\OneDrive - UvA\\UV-Vis\\Datafiles\\Export3d_20 min\\Figures_TIC\\"*filename*".png")

	return
end


function MultipleWavelengthPeakDetection(trw,wv,signal,minInt, deltaTime, minLambdaCount)
	#Calculate the 1st and 2nd derivative
	tr_d1, signal_d1 = deriv(trw, signal)
	tr_d2, signal_d2 = deriv(tr_d1, signal_d1)

	#Peaks detected in the 2nd derivative
	#add prominence
	#add minimum intensity?
	peaks = zeros(size(signal,1),size(signal,2))
#	for c = 1:size(signal,2)
#		#original data
#		peaks[findpeaks(signal[:,c],min_prom=5.),c,1] .= 1
#	end
#	for c = 1:size(signal,2)
#		prom = maximum(abs.(signal_d1))*0.003;
#		#Find positive peaks in 1st derivative
#		peaks[findpeaks(signal_d1[:,c],min_prom=prom),c,2] .= 1
#		#Find negative peaks in 1st derivative
#		peaks[findpeaks(-1*signal_d1[:,c],min_prom=prom),c,2] .= -1
#		#* indexing in row 1 is actually 1.5
#	end
	prom = minPeakPromCalc(minInt, trw,signal)
	for c = 1:size(signal,2)									#Var
		#Find positive peaks in 2nd derivative
		#peaks[findpeaks(signal_d2[:,c],min_prom=prom).+1,c] .= 1
		#Find negative peaks in 2nd derivative
		peaks[findpeaks(-1*signal_d2[:,c],min_prom=prom).+1,c] .= 1
	end

	#Cleanup based on the minimum signal intensity & single points
	#peaks_filt = filterPeaks(tr, signal, peaks, minInt)
	peaks_filt = peaks .* (signal .>= minInt)

	peaks_sum = sum(peaks_filt,dims=2)
	#Gouping
	peak_list = MWPDgrouping(peaks_filt, trw, wv, signal, deltaTime, minLambdaCount)

	if isnothing(peak_list)
		return
	end
	#background flagging peak detection -> correlation with neighbouring region
	peak_list = backgroundFlagging(peak_list, trw, signal, peaks_sum)


	return peak_list
end


function filterPeaks(trw, signal, peaks, minInt)
	windStraal = Int(floor(findfirst(trw .>= 0.5)/2))
	inds = findall(peaks .== 1)
	pkf = zeros(size(peaks))
	for ind in inds
		if ind[1] <= windStraal
			window = [1,ind[1]+windStraal]
			mid = ind[1]
		elseif ind[1] + windStraal > length(trw)
			window = [ind[1] - windStraal,length(trw)]
			mid = windStraal + 1
		else
			window = [ind[1] - windStraal,ind[1] + windStraal]
			mid = windStraal + 1
		end
		sig = signal[window[1]:window[2], ind[2]]
		fl = mid
		fr = mid
		#left search
		while fl-1 >= 1 && sig[fl-1] > sig[fl]
			fl -= 1
		end
		while fr + 1 <= length(sig) && sig[fr+1] > sig[fr]
			fr += 1
		end
		while fl-1 >= 1 && sig[fl-1] < sig[fl]
			fl -= 1
		end
		while fr + 1 <= length(sig) && sig[fr+1] < sig[fr]
			fr += 1
		end
		if (sig[mid] - median(sig[fl:fr])) >= minInt
			pkf[ind] = 1
		end
	end
	return pkf
end



function MWPDgrouping(peaks_filt, trw, wv, signal, deltaTime, minLambdaCount)
	#Linking of datapoints
	#%assign peaks from highest count(cumpeaks) to lowest
	group = zeros(size(peaks_filt));
	rest = zeros(size(peaks_filt));
	tempeaks = copy(peaks_filt);
	num = 1


	while any(sum(tempeaks .> 0, dims=2) .> 1)
		#%find peaks for the higher wavelengths
		idl = findlast(sum(tempeaks .> 0, dims=1).> 0);
		idl = idl[2]
		idt = findfirst(tempeaks[:,idl] .> 0);
		group[idt,idl] = num;
		tempeaks[idt,idl] = 0;
		#%link lower wavelengths
		while idl > 1
			idl = idl - 1;
			if tempeaks[idt,idl] > 0
				group[idt,idl] = num;
				tempeaks[idt,idl] = 0;
			elseif tempeaks[idt + 1,idl] > 0
				idt = idt + 1;
				group[idt,idl] = num;
				tempeaks[idt,idl] = 0;
			elseif tempeaks[idt - 1,idl] > 0
				idt = idt - 1;
				group[idt,idl] = num;
				tempeaks[idt,idl] = 0;
			else
				break
			end
		end
		if sum(group .== num) >= minLambdaCount
			num = num + 1;
		else
			group[group .== num] .= 0
		end
	end


	#Merging of groups
	if maximum(group) == 0
		println("Error: No peaks were found")
		return
	end


	#Setup extracted information dataframe
	lambda = zeros(Int(maximum(group)),size(group,2))
	#peak_list = DataFrame(tStart = zeros(Int(maximum(group)),1), tEnd = zeros(Int(maximum(group)),1), numPoints = zeros(Int(maximum(group)),1))
	peak_list = DataFrame(peakNum = 1:Int(maximum(group)), t_avg = NaN, maxInt = NaN, maxWavelength = NaN, tStart = NaN, tEnd = NaN, numPoints = NaN, FlagCor = NaN, FlagDot = NaN)
	for gr = 1:Int(maximum(group))
		lambda[gr,:] = sum(group .== gr, dims=1)
		peak_list[!,"numPoints"][gr] = sum(lambda[gr,:])
		peak_list[!,"tStart"][gr] = findfirst(group .== gr)[1]
		peak_list[!,"tEnd"][gr] = findlast(group .== gr)[1]
		t_avg = sum(trw .* sum(group .== gr, dims=2))/sum(group .== gr)
		peak_list[!,"t_avg"][gr] = t_avg
	end


	#Calculate potential merging candidates
	lambda_check = ones(size(peak_list,1),size(peak_list,1)).*NaN
	for i = 1:size(peak_list,1)
		lambda_check[vec(sum((lambda .+ lambda[i,:]') .== 2, dims = 2) .== 0),i] .= 1
		lambda_check[:,i] = lambda_check[:,i].*(abs.(peak_list[!,"t_avg"] .- peak_list[!,"t_avg"][i]))
		lambda_check[1:i,i] .= NaN
	end
	lambda_check[isnan.(lambda_check)] .= Inf


	#Merge groups with low tr difference
	while any(lambda_check .< deltaTime)
		#Locate point with minimum retention difference
		min_tr = findmin(lambda_check)[1]
		min_ind = findall(lambda_check .== min_tr)
		#locate the biggest group
		min_ind = min_ind[findmax(map(x -> peak_list[!,"numPoints"][min_ind[x][1]],1:size(min_ind,1)))[2]]

		#Merge column to row
		gr = min_ind[1]
		lambda[gr,:] = lambda[gr,:] .+ lambda[min_ind[2],:]
		peak_list[!,"t_avg"][gr] = ((peak_list[!,"t_avg"][gr]*peak_list[!,"numPoints"][gr]) + (peak_list[!,"t_avg"][min_ind[2]]*peak_list[!,"numPoints"][min_ind[2]]))/(peak_list[!,"numPoints"][gr] + peak_list[!,"numPoints"][min_ind[2]])
		peak_list[!,"numPoints"][gr] = peak_list[!,"numPoints"][gr] + peak_list[!,"numPoints"][min_ind[2]]
		#peak_list[!,"tStart"][gr] = findfirst(group .== gr)[1]
		#peak_list[!,"tEnd"][gr] = findlast(group .== gr)[1]

		#Set column information to NaN
		peak_list[!,"numPoints"][min_ind[2]] = NaN

		#set lambda information to Inf
		lambda_check[lambda_check[:,min_ind[2]].==Inf,min_ind[1]] .=Inf
		lambda_check[:,min_ind[2]] .= Inf
	end

	#Remove empty rows from list
	peak_list = peak_list[isnan.(peak_list[!,"numPoints"]) .== 0,:]

	for i = 1:size(peak_list,1)
		peak_list[!,"peakNum"][i] = i
		ind_t = findmin(abs.(trw .- peak_list[!,"t_avg"][i]))[2]
		maxInt, indMax= findmax(signal[ind_t,:])
		peak_list[!,"maxInt"][i] = maxInt
		peak_list[!,"maxWavelength"][i] = wv[indMax]
	end


	return peak_list
end



function BackgroundEstimation(signal,type)
	if type == "MovingAvg"
		bg = zeros(size(signal))
		minL = Int(1 - floor(size(signal,1)/10))
		maxL = Int(1 + floor(size(signal,1)/10))
		for w = 1:size(signal,1)
			if minL < 1
				bg[w,:] = mean(signal[1:maxL,:],dims=1)
			elseif maxL > size(signal,1)
				bg[w,:] = mean(signal[minL:size(signal,1),:],dims=1)
			else
				bg[w,:] = mean(signal[minL:maxL,:],dims=1)
			end
			minL += 1
			maxL += 1
		end
	end
	return bg
end


function backgroundFlagging(peak_list, trw, signal, peaks_sum)
	TIC = sum(signal, dims=2)
	z = AsLS(TIC, 100000, 0.001)

	sig_adj = (signal .- minimum(signal))./maximum(signal .- minimum(signal))

	for p = 1:size(peak_list,1)
		ind_pk = findmin(abs.(trw .- peak_list[!,"t_avg"][p]))[2]
		ind_bg = findlast(TIC[1:ind_pk] .< z[1:ind_pk])
		if isnothing(ind_bg)
			ind_bg = findfirst(TIC[ind_pk:end] .< z[ind_pk:end])
			if isnothing(ind_bg)
				peak_list[!,"FlagCor"][p] = NaN
				peak_list[!,"FlagDot"][p] = NaN
				continue
			end
		end
		corr = cor(signal[ind_bg,:]./(maximum(signal[ind_bg,:])),signal[ind_pk,:]./maximum(signal[ind_pk,:]))
		peak_list[!,"FlagCor"][p] = 1-corr^2
		dott = dot(signal[ind_bg,:],signal[ind_pk,:])/sqrt(sum(signal[ind_bg,:].^2)*sum(signal[ind_pk,:].^2))
		peak_list[!,"FlagDot"][p] = dott^2
	end

	return peak_list
end


## General functions



function deriv(x,z)
	#where x equals a vector containing the time and z contains the signal
	z_d = diff(z, dims = 1)./diff(x, dims = 1)
	x_d = x[2:end,1] - 0.5*diff(x, dims = 1)
	return x_d, z_d
end




function AsLS(y, lambda, p)
	y = vec(y)

	# Estimate baseline with asymmetric least squares
	m = length(y)
	d = zeros(m,m)
	d[diagind(d)] .= 1
	D = diff(diff(d, dims = 1),dims =1)
	w = ones(m, 1)
	z = []

	for it = 1:10
		W = spdiagm(m, m, 0 => vec(w))
	    C = cholesky(W + lambda * D' * D)
	    z = C.U \ (C.U' \ (w .* y))
	    w = p * (y .> z) + (1 - p) * (y .< z)
	end

	return z
end



function minPeakPromCalc(minInt, trw, signal)
	#find highest peak in chrom
	sig = zeros(size(signal[:,1]))
	med = mean(sum(signal,dims =1))
	for c = 1:size(signal,2)
		if c != 1 && sum(signal[:,c]) < med
			continue
		end
		z = AsLS(signal[:,c],100000,0.01)
		# if c == 1
		# 	sig = signal[:,c] .- z
		# else
		if maximum(signal[:,c] .- z) > maximum(sig)
			sig = signal[:,c] .- z
		end
		if maximum(sig) .> minInt
			break
		end
	end
	# sig = signal[:,findall(sum(signal,dims=1) .> median(sum(signal,dims =1)))]
	maxInt, ind = findmax(sig)
	#get start and end of FWHH
	if findlast(sig[1:ind[1]] .< maxInt/2) == nothing
		st = 1
	else
		st = findlast(sig[1:ind[1]] .< maxInt/2)
	end
	if findfirst(sig[ind[1]:end].< maxInt/2) == nothing
		en = length(trw)
	else
		en = ind[1] + findfirst(sig[ind[1]:end].< maxInt/2) -1
	end
	#Setup a peak for minimum intensity case
	hwhm = trw[en] - trw[st]
	x = -2.5*hwhm:4*hwhm/30:2.5*hwhm
	y = minInt* exp.(-log(2) .* (x./hwhm).^2 )
	#Calc derivative and peak prominence
	dx,dy = deriv(x,y)
	ddx,ddy = deriv(dx,dy)

	minProm = abs(minimum(ddy) - maximum(ddy))

	return minProm
end



#temp function
function plotResults(peak_list, trw, signal, lambda, file, path)
	#Check directory
	if ~isdir(path*"figures")
		mkdir(path*"figures")
	end
	savepath = path*"figures\\"*file[1:findlast('.',file)-1]*"\\"
	if ~isdir(savepath)
		mkdir(savepath)
	elseif size(readdir(savepath))[1] != 0
		#clear the folder
		rm.(savepath.*readdir(savepath))
	end

	#plot Total Intensity Chromatogram
	plot(trw[50:end,:], sum(signal[50:end,:], dims=2),legend=false)
	temp  = ones(size(peak_list,1),2).*NaN
	for i = 1:size(peak_list,1)
		temp[i,1] = peak_list[!,"t_avg"][i]
		if sum(signal, dims=2)[findfirst(peak_list[!,"t_avg"][i] .< trw)] .> -100
			temp[i,2] = sum(signal, dims=2)[findfirst(peak_list[!,"t_avg"][i] .< trw)]
		else
			temp[i,2] = 0
		end
	#	plot!(peak_list[!,"t_avg"][i],sum(signal, dims=2)[findfirst(peak_list[!,"t_avg"][i] .== tr)], seriestype = :scatter)
	end
	plot!(temp[:,1],temp[:,2],seriestype = :scatter,markersize = 2)
	xlabel!("Time (min)")
	ylabel!("Intensty")
	#ylims!(-10,maximum(sum(signal[50:end,:],dims=2)))
	savefig(savepath*"TIC.png")

	#heatmap
	signaladj = copy(signal)
	signaladj[signaladj .> 150] .= 150
	heatmap(lambda, trw[50:end], signaladj[50:end,:], size=(1200,800), c = :thermal)
	savefig(savepath*"heatmap_zoomed.png")

	#heatmap
	signaladj = copy(signal)
	signaladj[signaladj .> 150] .= 150
	heatmap(lambda, trw, signaladj, size=(1200,800), c = :thermal)
	savefig(savepath*"heatmap.png")

	for i = 1:size(peak_list,1)
		#plot spectrum for each deatected peak
		ind = findmin(abs.(trw .- peak_list[!,"t_avg"][i]))[2]
		plot(lambda,signal[ind,:], label=string(peak_list[!,"t_avg"][i])*" Flagged = "*string(peak_list[!,"FlagCor"][i]))
		xlabel!("Wavelength (lambda)")
		ylabel!("Intensty")
		savefig(savepath*"peak$i"*".png")
	end
end




#
# ########################################################################################
# # Testing area
# include("DAD_Import.jl")
# bgfile = "029-P2-B11-Blank.CSV"
# path = "C:\\Users\\dherwer\\OneDrive - UvA\\UV-Vis\\Datafiles\\Export3d_20 min\\"
# tw = 21
# # peaks = DADPeakDetectionBatch(path,minInt,deltaTime,minWV,type; MovingAvg =true, time_window = tw)
# peaks = DADPeakDetectionBatch(path,minInt,deltaTime,minWV,type; filename_bg = bgfile, time_window = tw, Deconvolution = "direct")
# bgfile = "031-P2-B11-Blank.CSV"
# path = "C:\\Users\\dherwer\\OneDrive - UvA\\UV-Vis\\Datafiles\\Export3d_10 min\\"
# tw = 11
# peaks = DADPeakDetectionBatch(path,minInt,deltaTime,minWV,type; filename_bg = bgfile, time_window = tw)
# bgfile = "004-P2-B11-Blank.CSV"
# path = "C:\\Users\\dherwer\\OneDrive - UvA\\UV-Vis\\Datafiles\\Export3d_5 min\\"
# tw = 5.99
# peaks = DADPeakDetectionBatch(path,minInt,deltaTime,minWV,type; filename_bg = bgfile, time_window = tw)
# bgfile = "035-P2-B11-Blank.CSV"
# path = "C:\\Users\\dherwer\\OneDrive - UvA\\UV-Vis\\Datafiles\\Export3d_2 5 min\\"
# tw = 3.49
# peaks = DADPeakDetectionBatch(path,minInt,deltaTime,minWV,type; filename_bg = bgfile, time_window = tw)
# bgfile = "035-P2-B11-Blank.CSV"
# path = "C:\\Users\\dherwer\\OneDrive - UvA\\UV-Vis\\Datafiles\\Export3d_1 min\\"
# tw = 1.99
# peaks = DADPeakDetectionBatch(path,minInt,deltaTime,minWV,type; filename_bg = bgfile, time_window = tw)
# minInt = 3			#minimum required intensity of the peak
# deltaTime = 0.005			#Time maximum time difference allowed for matching peak tops between different wavelengths
# minWV = 5 				#Minimum wavelength range required for a group of peak tops to be accepted as a part of an spectrum (in nm)
# type = "MWPD"				#deconvolution time
# ft = 0.1
# #
# #
# ## processing single file with backgroun correction
# peaks = DADPeakDetection(filename,path,minInt,deltaTime,minWV,type; filename_bg = bgfile, time_window = tw)
#
# ##batchprocessing for a single folder
# peaks = DADPeakDetectionBatch(path,minInt,deltaTime,minWV,type; filename_bg = bgfile, time_window = tw)
# peaks = DADPeakDetectionBatch(path,minInt,deltaTime,minWV,type)
#
#
#
#
#
# ##kwargs (optional parameters)
# # filename_bg = "029-P2-B11-Blank.CSV"
# # path_bg = "path to back ground file"							#if not provided the path of the datafile will be used
# tr_bg, signal_bg = importDAD(bgfile, path)
#
# filename = "006-P2-B9-25_peak list.CSV"
# trw, signal = importDAD(filename, path)
#
# # #Tijmens files
# filename = "012-P1-E1-Dye 1-25 MegaMix.CSV"
# path = "C:\\Users\\dherwer\\OneDrive - UvA\\UV-Vis\\Datafiles\\Tijmen\\9 min"
# minInt = 30				#minimum required intensity of the peak
# deltaTime = 0.05			#Time maximum time difference allowed for matching peak tops between different wavelengths
# minWV = 5 				#Minimum wavelength range required for a group of peak tops to be accepted as a part of an spectrum (in nm)
# type = "MWPD"
# peaks = DADPeakDetection(filename,path,minInt,deltaTime,minWV,type; filename_bg = "011-NV-Blank.CSV",time_window = 12.2)





# filename = "measurement3.CSV"
# filename = "measurement10.CSV"
# path = "O:\\Boelrijk_Jim\\AutoLC\\past_runs\\test_run4_concomps\\"
# tw = 25-5
# tr, signal, wv = importDAD(filename,path)
# tr2, signal2, wv2 = importDAD(filename2,path)
# heatmap(signal)
# heatmap(signal2)
# peaks = DADPeakDetection(filename,path,minInt,deltaTime,minWV,type; time_window = tw, FlagThreshold = ft)
