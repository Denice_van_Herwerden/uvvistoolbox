using Findpeaks, DataFrames, Statistics, LinearAlgebra, SparseArrays

#temp
using Plots




function DADPeakDetection(filename,path, minInt, deltaTime, minWV, type; kwargs...)
	#kwargs: filename_bg, path_bg, time_window

	# unpacking kwargs
	while any(keys(kwargs) .== :kwargs)
		kwargs = kwargs[:kwargs]
	end

	#import DAD file
	tr, signal, wv = importDAD(filename,path)
	minLambdaCount = floor(minWV/mean(diff(wv)))			#number of datapoints required to fullfill the minimum peak broadness in the wavelngth domain

	#Check orientation of the time and signal variable
	if size(tr,1) == 1
		tr = tr'
	end

	if size(tr,1) != size(signal,1)
		signal = signal'
	end


	#Check for optional background correction method
	if any(keys(kwargs) .== :filename_bg)
		if any(keys(kwargs) .== :path_bg)
			tr_bg, signal_bg = importDAD(kwargs[:filename_bg], kwargs[:path_bg])
		else
			tr_bg, signal_bg = importDAD(kwargs[:filename_bg], path)
		end
		#check if substraction is possible
		if size(signal) == size(signal_bg)
			signal = signal .- signal_bg
		else
			println("Background correction skipped due to mismatching size")
		end
	end


	#Check time window cutting
	if any(keys(kwargs) .== :time_window)
		#check input for time window
		if any(typeof(kwargs[:time_window]) .== [Int64 Float64])
			#1 number is end of chrom
			if minimum(tr) .> kwargs[:time_window]
				println("Time window not adjusted, since first time point is larger then provided time window")
			else
				maxInd = findlast(tr .< kwargs[:time_window])
				tr = tr[1:maxInd]
				signal = signal[1:maxInd,:]
				println("Time range will be adjust to "*string(minimum(tr))*" - "*string(kwargs[:time_window]))
			end
		elseif size(kwargs[:time_window]) == (1, 2) || size(kwargs[:time_window]) == (1, 2)
			#2 numbers is range
			minTime = minimum(kwargs[:time_window])
			maxTime = maximum(kwargs[:time_window])

			if minTime .> maximum(tr) || maxTime .< minimum(tr)
				println("Selected window range is out of range for the chromatogram")
			else
				minInd = findfirst(tr .> minTime)
				maxInd = findlast(tr .< maxTime)
				tr = tr[minInd:maxInd]
				signal = signal[minInd:maxInd,:]
				println("Time range will be adjust to "*string(tr[minInd])*"-"*string(tr[maxInd]))
			end
		else
			#error check input
			println("Provided input for time_window not compatible. Either provide a single number for the maximum time (time_window = 5.3) or an array containing the minimum and maximum time (time_window = [1.6 4.3])")
		end
	end


	#Perform peak detection	!!!!!!!!!!!!!!!!temp!!!!!!!!!!(figures false kwarg)
	if type == "MWPD"
		#Multiple wavelength peak detection
		peak_list = MultipleWavelengthPeakDetection(tr,wv,signal,minInt, deltaTime, minLambdaCount)
	elseif type == "TIC"
		peak_list = TICPeakDetection(tr, wv, signal, minInt)
	else
		println("The peak detection method is unknown. Please choose one of the following options:
				 1) \"MWPD\": Multiple wavelength peak detection")
	end


	#Save peak list
	if isnothing(peak_list)
		if isfile(path*"\\"*filename[1:findlast('.',filename)-1]*"_peak list.csv")
			rm(path*"\\"*filename[1:findlast('.',filename)-1]*"_peak list.csv")
		end
	else
		CSV.write(path*"\\"*filename[1:findlast('.',filename)-1]*"_peak list.csv", peak_list)
		###temp###
		if type == "MWPD" && all(keys(kwargs) .!= :figures)
			plotResults(peak_list, tr, signal, wv, filename, path)
		end
	end


	return peak_list
end



#Improve by runnig parallel
function DADPeakDetectionBatch(path, minInt, deltaTime, minLambdaCount, type; kwargs...)
	#process multiple files in the dame directory
	if any(keys(kwargs) .== :filenames)
		files = kwargs[:filenames]
	else
		files = readdir(path)
	end
	for f = 1:length(files)
		if isfile(path*"\\"*files[f]) && (cmp(files[f][end-12:end],"peak list.csv") != 0)
			if isempty(kwargs.data)
				DADPeakDetection(files[f],path, minInt, deltaTime, minLambdaCount, type)
			elseif typeof(kwargs[:filename_bg]) == String
				#same blank for all measurements
				if cmp(files[f], kwargs[:filename_bg]) == 0
					println("Skipped background measurement "*files[f])
					continue
				end
				println("Processing "*files[f]*" with blank removal")
				DADPeakDetection(files[f],path, minInt, deltaTime, minLambdaCount, type; kwargs)
			end
		end
	end
end



## Peak detection functions

function TICPeakDetection(tr, wv, signal, percInt,filename) 	#rm filename after temp
	#currently evaluating if TIC peak detection detects more/less peaks
	if size(tr) != size(signal)
		TIC = sum(signal, dims=2)
	end
	tr_d1, TIC_d1 = deriv(tr, TIC)
	tr_d2, TIC_d2 = deriv(tr_d1, TIC_d1)

	#Detect peak maxima
	prom = minPeakPromCalc(max(TIC)*percInt,tr,TIC)
	#Find negative peaks in 2nd derivative
	peaks = zeros(size(TIC))
	peaks[findpeaks(-1*TIC_d2[:,1],min_prom=prom*size(signal,2)).+1,1] .= -1
	#Cleanup based on the minimum signal intensity & single points
	peaks_filt = ((peaks .== -1) .+ (TIC .>= minInt)) .== 2
	sum(peaks_filt)

	plot(peaks)
	plot!(TIC./maximum(TIC))

	savefig("C:\\Users\\dherwer\\OneDrive - UvA\\UV-Vis\\Datafiles\\Export3d_20 min\\Figures_TIC\\"*filename*".png")

	return
end


function MultipleWavelengthPeakDetection(tr,wv,signal,minInt, deltaTime, minLambdaCount)
	#Calculate the 1st and 2nd derivative
	tr_d1, signal_d1 = deriv(tr, signal)
	tr_d2, signal_d2 = deriv(tr_d1, signal_d1)

	#Peaks detected in the 2nd derivative
	#add prominence
	#add minimum intensity?
	peaks = zeros(size(signal,1),size(signal,2))
#	for c = 1:size(signal,2)
#		#original data
#		peaks[findpeaks(signal[:,c],min_prom=5.),c,1] .= 1
#	end
#	for c = 1:size(signal,2)
#		prom = maximum(abs.(signal_d1))*0.003;
#		#Find positive peaks in 1st derivative
#		peaks[findpeaks(signal_d1[:,c],min_prom=prom),c,2] .= 1
#		#Find negative peaks in 1st derivative
#		peaks[findpeaks(-1*signal_d1[:,c],min_prom=prom),c,2] .= -1
#		#* indexing in row 1 is actually 1.5
#	end
	prom = minPeakPromCalc(minInt, tr,signal)
	for c = 1:size(signal,2)									#Var
		#Find positive peaks in 2nd derivative
		#peaks[findpeaks(signal_d2[:,c],min_prom=prom).+1,c] .= 1
		#Find negative peaks in 2nd derivative
		peaks[findpeaks(-1*signal_d2[:,c],min_prom=prom).+1,c] .= -1
	end

	#Cleanup based on the minimum signal intensity & single points
	peaks_filt = ((peaks .== -1) .+ (signal .>= minInt)) .== 2


	peaks_sum = sum(peaks_filt,dims=2)
	#Gouping
	peak_list = MWPDgrouping(peaks_filt, tr, wv, signal, deltaTime, minLambdaCount)

	if isnothing(peak_list)
		return
	end
	#background flagging peak detection -> correlation with neighbouring region
	peak_list = backgroundFlagging(peak_list, tr, signal, peaks_sum)


	return peak_list
end




function MWPDgrouping(peaks_filt, tr, wv, signal, deltaTime, minLambdaCount)
	#Linking of datapoints
	#%assign peaks from highest count(cumpeaks) to lowest
	group = zeros(size(peaks_filt));
	rest = zeros(size(peaks_filt));
	tempeaks = copy(peaks_filt);
	num = 1


	while any(sum(tempeaks .> 0, dims=2) .> 1)
		#%find peaks for the higher wavelengths
		idl = findlast(sum(tempeaks .> 0, dims=1).> 0);
		idl = idl[2]
		idt = findfirst(tempeaks[:,idl] .> 0);
		group[idt,idl] = num;
		tempeaks[idt,idl] = 0;
		#%link lower wavelengths
		while idl > 1
			idl = idl - 1;
			if tempeaks[idt,idl] > 0
				group[idt,idl] = num;
				tempeaks[idt,idl] = 0;
			elseif tempeaks[idt + 1,idl] > 0
				idt = idt + 1;
				group[idt,idl] = num;
				tempeaks[idt,idl] = 0;
			elseif tempeaks[idt - 1,idl] > 0
				idt = idt - 1;
				group[idt,idl] = num;
				tempeaks[idt,idl] = 0;
			else
				break
			end
		end
		if sum(group .== num) >= minLambdaCount
			num = num + 1;
		else
			group[group .== num] .= 0
		end
	end


	#Merging of groups
	if maximum(group) == 0
		println("Error: No peaks were found")
		return
	end


	#Setup extracted information dataframe
	lambda = zeros(Int(maximum(group)),size(group,2))
	#peak_list = DataFrame(tStart = zeros(Int(maximum(group)),1), tEnd = zeros(Int(maximum(group)),1), numPoints = zeros(Int(maximum(group)),1))
	peak_list = DataFrame(peakNum = 1:Int(maximum(group)), t_avg = NaN, maxInt = NaN, maxWavelength = NaN, tStart = NaN, tEnd = NaN, numPoints = NaN, FlagCor = NaN, FlagDot = NaN)
	for gr = 1:Int(maximum(group))
		lambda[gr,:] = sum(group .== gr, dims=1)
		peak_list[!,"numPoints"][gr] = sum(lambda[gr,:])
		peak_list[!,"tStart"][gr] = findfirst(group .== gr)[1]
		peak_list[!,"tEnd"][gr] = findlast(group .== gr)[1]
		t_avg = sum(tr .* sum(group .== gr, dims=2))/sum(group .== gr)
		peak_list[!,"t_avg"][gr] = t_avg
	end


	#Calculate potential merging candidates
	lambda_check = ones(size(peak_list,1),size(peak_list,1)).*NaN
	for i = 1:size(peak_list,1)
		lambda_check[vec(sum((lambda .+ lambda[i,:]') .== 2, dims = 2) .== 0),i] .= 1
		lambda_check[:,i] = lambda_check[:,i].*(abs.(peak_list[!,"t_avg"] .- peak_list[!,"t_avg"][i]))
		lambda_check[1:i,i] .= NaN
	end
	lambda_check[isnan.(lambda_check)] .= Inf


	#Merge groups with low tr difference
	while any(lambda_check .< deltaTime)
		#Locate point with minimum retention difference
		min_tr = findmin(lambda_check)[1]
		min_ind = findall(lambda_check .== min_tr)
		#locate the biggest group
		min_ind = min_ind[findmax(map(x -> peak_list[!,"numPoints"][min_ind[x][1]],1:size(min_ind,1)))[2]]

		#Merge column to row
		gr = min_ind[1]
		lambda[gr,:] = lambda[gr,:] .+ lambda[min_ind[2],:]
		peak_list[!,"t_avg"][gr] = ((peak_list[!,"t_avg"][gr]*peak_list[!,"numPoints"][gr]) + (peak_list[!,"t_avg"][min_ind[2]]*peak_list[!,"numPoints"][min_ind[2]]))/(peak_list[!,"numPoints"][gr] + peak_list[!,"numPoints"][min_ind[2]])
		peak_list[!,"numPoints"][gr] = peak_list[!,"numPoints"][gr] + peak_list[!,"numPoints"][min_ind[2]]
		#peak_list[!,"tStart"][gr] = findfirst(group .== gr)[1]
		#peak_list[!,"tEnd"][gr] = findlast(group .== gr)[1]

		#Set column information to NaN
		peak_list[!,"numPoints"][min_ind[2]] = NaN

		#set lambda information to Inf
		lambda_check[lambda_check[:,min_ind[2]].==Inf,min_ind[1]] .=Inf
		lambda_check[:,min_ind[2]] .= Inf
	end

	#Remove empty rows from list
	peak_list = peak_list[isnan.(peak_list[!,"numPoints"]) .== 0,:]

	for i = 1:size(peak_list,1)
		peak_list[!,"peakNum"][i] = i
		ind_t = findmin(abs.(tr .- peak_list[!,"t_avg"][i]))[2]
		maxInt, indMax= findmax(signal[ind_t,:])
		peak_list[!,"maxInt"][i] = maxInt
		peak_list[!,"maxWavelength"][i] = wv[indMax]
	end


	return peak_list
end





function backgroundFlagging(peak_list, tr, signal, peaks_sum)
	TIC = sum(signal, dims=2)
	z = AsLS(TIC, 100000, 0.001)

	for p = 1:size(peak_list,1)
		ind_pk = findmin(abs.(tr .- peak_list[!,"t_avg"][p]))[2]
		ind_bg = findlast(TIC[1:ind_pk] .< z[1:ind_pk])
		if isnothing(ind_bg)
			ind_bg = findfirst(TIC[ind_pk:end] .< z[ind_pk:end])
			if isnothing(ind_bg)
				peak_list[!,"FlagCor"][p] = NaN
				peak_list[!,"FlagDot"][p] = NaN
				continue
			end
		end
		corr = cor(signal[ind_bg,:]./(maximum(signal[ind_bg,:])),signal[ind_pk,:]./maximum(signal[ind_pk,:]))
		peak_list[!,"FlagCor"][p] = 1-corr^2
		dott = dot(signal[ind_bg,:]./(maximum(signal[ind_bg,:])),signal[ind_pk,:]./maximum(signal[ind_pk,:]))
		peak_list[!,"FlagDot"][p] = 1-dott^2
	end

	# r2 = zeros(size(signal,1),1)
	# for j = 1:size(peak_list,1)
	# 	for i = 1:size(signal,1)
	# 		r2[i] = cor(signal[i,:],signal[Int(peak_list[!,"tStart"][j]),:])
	# 	end
	# 	peak_list[!,"Flag"][j] = median(r2.^2)
	# end

	return peak_list
end


## General functions



function deriv(x,z)
	#where x equals a vector containing the time and z contains the signal
	z_d = diff(z, dims = 1)./diff(x, dims = 1)
	x_d = x[2:end,1] - 0.5*diff(x, dims = 1)
	return x_d, z_d
end


function AsLS(y, lambda, p)
	y = vec(y)

	# Estimate baseline with asymmetric least squares
	m = length(y)
	d = zeros(m,m)
	d[diagind(d)] .= 1
	D = diff(diff(d, dims = 1),dims =1)
	w = ones(m, 1)
	z = []

	for it = 1:10
		W = spdiagm(m, m, 0 => vec(w))
	    C = cholesky(W + lambda * D' * D)
	    z = C.U \ (C.U' \ (w .* y))
	    w = p * (y .> z) + (1 - p) * (y .< z)
	end

	return z
end



function minPeakPromCalc(minInt, tr, signal)
	#find 4 hihest peaks in chrom
	maxInt, ind = findmax(signal)
	st = findlast(signal[1:ind[1],ind[2]] .< maxInt/2)
	en = ind[1] + findfirst(signal[ind[1]:end,ind[2]].< maxInt/2) -1
	#Setup a peak for minimum intensity case
	hwhm = tr[en] - tr[st]
	x = -2.5*hwhm:4*hwhm/30:2.5*hwhm
	y = minInt* exp.(-log(2) .* (x./hwhm).^2 )
	#Calc derivative and peak prominence
	dx,dy = deriv(x,y)
	ddx,ddy = deriv(dx,dy)

	minProm = abs(minimum(ddy) - maximum(ddy))

	return minProm
end



#temp function
function plotResults(peak_list, tr, signal, lambda, file, path)
	#Check directory
	if ~isdir(path*"\\figures")
		mkdir(path*"\\figures")
	end
	savepath = path*"\\figures\\"*file[1:findlast('.',file)-1]
	if ~isdir(savepath)
		mkdir(savepath)
	elseif size(readdir(savepath))[1] != 0
		#clear the folder
		rm.(savepath*"\\".*readdir(savepath))
	end

	#plot Total Intensity Chromatogram
	plot(tr[50:end,:], sum(signal[50:end,:], dims=2),legend=false)
	temp  = ones(size(peak_list,1),2).*NaN
	for i = 1:size(peak_list,1)
		temp[i,1] = peak_list[!,"t_avg"][i]
		if sum(signal, dims=2)[findfirst(peak_list[!,"t_avg"][i] .< tr)] .> -100
			temp[i,2] = sum(signal, dims=2)[findfirst(peak_list[!,"t_avg"][i] .< tr)]
		else
			temp[i,2] = 0
		end
	#	plot!(peak_list[!,"t_avg"][i],sum(signal, dims=2)[findfirst(peak_list[!,"t_avg"][i] .== tr)], seriestype = :scatter)
	end
	plot!(temp[:,1],temp[:,2],seriestype = :scatter,markersize = 2)
	xlabel!("Time (min)")
	ylabel!("Intensty")
	#ylims!(-10,maximum(sum(signal[50:end,:],dims=2)))
	savefig(savepath*"\\TIC.png")

	#heatmap
	signaladj = copy(signal)
	signaladj[signaladj .> 150] .= 150
	heatmap(lambda, tr[50:end], signaladj[50:end,:], size=(1200,800), c = :thermal)
	savefig(savepath*"\\heatmap_zoomed.png")

	#heatmap
	signaladj = copy(signal)
	signaladj[signaladj .> 150] .= 150
	heatmap(lambda, tr, signaladj, size=(1200,800), c = :thermal)
	savefig(savepath*"\\heatmap.png")

	for i = 1:size(peak_list,1)
		#plot spectrum for each deatected peak
		ind = findmin(abs.(tr .- peak_list[!,"t_avg"][i]))[2]
		plot(lambda,signal[ind,:], label=string(peak_list[!,"t_avg"][i])*" Flagged = "*string(peak_list[!,"FlagCor"][i]))
		xlabel!("Wavelength (lambda)")
		ylabel!("Intensty")
		savefig(savepath*"\\peak$i"*".png")
	end
end




#
# ########################################################################################
# # Testing area
# include("DAD_Import.jl")
# bgfile = "029-P2-B11-Blank.CSV"
# path = "C:\\Users\\dherwer\\OneDrive - UvA\\UV-Vis\\Datafiles\\Export3d_20 min"
# tw = 21
# peaks = DADPeakDetectionBatch(path,minInt,deltaTime,minWV,type; filename_bg = bgfile, time_window = tw)
# bgfile = "031-P2-B11-Blank.CSV"
# path = "C:\\Users\\dherwer\\OneDrive - UvA\\UV-Vis\\Datafiles\\Export3d_10 min"
# tw = 11
# peaks = DADPeakDetectionBatch(path,minInt,deltaTime,minWV,type; filename_bg = bgfile, time_window = tw)
# bgfile = "004-P2-B11-Blank.CSV"
# path = "C:\\Users\\dherwer\\OneDrive - UvA\\UV-Vis\\Datafiles\\Export3d_5 min"
# tw = 5.99
# peaks = DADPeakDetectionBatch(path,minInt,deltaTime,minWV,type; filename_bg = bgfile, time_window = tw)
# bgfile = "035-P2-B11-Blank.CSV"
# path = "C:\\Users\\dherwer\\OneDrive - UvA\\UV-Vis\\Datafiles\\Export3d_2 5 min"
# tw = 3.49
# peaks = DADPeakDetectionBatch(path,minInt,deltaTime,minWV,type; filename_bg = bgfile, time_window = tw)
# bgfile = "035-P2-B11-Blank.CSV"
# path = "C:\\Users\\dherwer\\OneDrive - UvA\\UV-Vis\\Datafiles\\Export3d_1 min"
# tw = 1.99
# peaks = DADPeakDetectionBatch(path,minInt,deltaTime,minWV,type; filename_bg = bgfile, time_window = tw)
# minInt = 0.5			#minimum required intensity of the peak
# deltaTime = 0.05			#Time maximum time difference allowed for matching peak tops between different wavelengths
# minWV = 5 				#Minimum wavelength range required for a group of peak tops to be accepted as a part of an spectrum (in nm)
# type = "MWPD"				#deconvolution time
#
#
#
# ## processing single file with backgroun correction
# peaks = DADPeakDetection(filename,path,minInt,deltaTime,minWV,type; filename_bg = bgfile, time_window = tw)
#
# ##batchprocessing for a single folder
# peaks = DADPeakDetectionBatch(path,minInt,deltaTime,minWV,type; filename_bg = bgfile, time_window = tw)
# peaks = DADPeakDetectionBatch(path,minInt,deltaTime,minWV,type)
#
#
#
#
#
# ##kwargs (optional parameters)
# # filename_bg = "029-P2-B11-Blank.CSV"
# # path_bg = "path to back ground file"							#if not provided the path of the datafile will be used
# tr_bg, signal_bg = importDAD(bgfile, path)
#
# # filename = "006-P2-B9-25_peak list.CSV"
# trw, signal = importDAD(filename, path)
#
# #Tijmens files
# filename = "012-P1-E1-Dye 1-25 MegaMix.CSV"
# path = "C:\\Users\\dherwer\\OneDrive - UvA\\UV-Vis\\Datafiles\\Tijmen\\9 min"
# minInt = 30				#minimum required intensity of the peak
# deltaTime = 0.05			#Time maximum time difference allowed for matching peak tops between different wavelengths
# minWV = 5 				#Minimum wavelength range required for a group of peak tops to be accepted as a part of an spectrum (in nm)
# type = "MWPD"
# peaks = DADPeakDetection(filename,path,minInt,deltaTime,minWV,type; filename_bg = "011-NV-Blank.CSV",time_window = 12.2)
