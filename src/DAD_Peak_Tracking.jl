using CSV, DataFrames, Statistics

using Plots


function DADpeakTracking(filename1,path1,filename2,path2,type,cor_thresh; kwargs...)
	#Function info
	#kwargs :pathout

	# # unpacking kwargs
	# while any(keys(kwargs) .== :kwargs)
	# 	kwargs = kwargs[:kwargs]
	# end

	#laod peak list 1
	if isfile(path1*"\\"*filename1)
		pkl1 = CSV.read(path1*"\\"*filename1,DataFrame)
	else
		println(path1*"\\"*filename1*" is not a valid peak list file")
		return
	end

	#load peak list 2
	if isfile(path2*"\\"*filename2)
		pkl2 = CSV.read(path2*"\\"*filename2,DataFrame)
	else
		println(path2*"\\"*filename2*" is not a valid peak list file")
		return
	end

	#Perform peak tracking
	if type == "simpleCorr"
		matched = simpleCorrelationMatching(pkl1,pkl2,cor_thresh)
	else
		println("The peak tracking method is unknown. Please choose one of the following options:
				 1) \"simpleCorr\": matching based on correlation only")
	end

	#plotMatches(matched, pkl1, pkl2, path1, filename1, filename2)

	#Save the matched list
	if any(keys(kwargs) .== :batch)
		return matched
	elseif any(keys(kwargs) .== :pathout)
		CSV.write(kwargs[:pathout]*"\\"*filename1[1:findlast('.',filename1)-1]*" "*filename2[1:findlast('.',filename2)-1]*"_peak tracking.csv",matched)
	else
		CSV.write(path1*"\\"*filename1[1:findlast('.',filename1)-1]*" "*filename2[1:findlast('.',filename2)-1]*"_peak tracking.csv",matched)
	end

	return matched
end



function DADpeakTrackingBatch(path,type,cor_thresh; kwargs...)
	files = readdir(path)
	files = files[contains.(files,"_peak list.csv")]
	if any(keys(kwargs).==:toMatch)
		file2match  = kwargs[:toMatch]
	else
		#use the file with most number of entries as reference
		ind = [0 0]
		for f = 1:length(files)
			length_data = size(CSV.read(path*files[f],DataFrame),1)
			if length_data .> ind[1]
				ind[1] = length_data
				ind[2] = f
			end
		end
		file2match = files[ind[2]]
	end

	#Setup dataframe for file2match
	pk_list0 = CSV.read(path*file2match,DataFrame)
	pk_num = DataFrame()
	pk_num[!,file2match] = float(pk_list0[!,"peakNum"])
	pk_tr = DataFrame()
	pk_tr[!,file2match] = pk_list0[!,"t_avg"]
	pk_spec = DataFrame()
	pk_spec[!,file2match] = pk_list0[!,"Spectrum"]

	#Match peaks to file2match
	for f = 1:length(files)
		if isfile(path*"\\"*files[f]) && contains(files[f],".csv")
			if files[f] .== file2match
				continue
			end
			#expand dataframe
			pk_num[!,files[f]] = NaN .* zeros(size(pk_num,1))
			pk_tr[!,files[f]] = NaN .* zeros(size(pk_tr,1))
			pk_spec[!,files[f]] = fill("",size(pk_spec,1))

			println("Matching: "*files[f])
			matched = DADpeakTracking(file2match,path,files[f],path,"simpleCorr",cor_thresh; batch = true)
			println("Adding peaks to master peak list")
			pk_num, pk_rt, pk_spec = mergePeakLists(files[f], pk_num, pk_tr, pk_spec, matched)
			println(files[f]*" Finished")
		end
	end

	pk_num_out, pk_tr_out, pk_spec_out = backwardsMatching(pk_num, pk_tr, pk_spec)

	CSV.write(path*"matched_peakNumber.csv",pk_num_out)
	CSV.write(path*"matched_retentionTime.csv",pk_tr_out)
	CSV.write(path*"matched_spectra.csv",pk_spec_out)

end



## Spectral extraction functions
function simpleCorrelationMatching(pkl1, pkl2, cor_thres)
	#Extract spectra from peak lists
	spec1 = spectraConvertion(pkl1)
	spec2 = spectraConvertion(pkl2)

	mat, mat_cor = correlationMatching(spec1,spec2)

	#Setup matched results with peak_num1/2, tr1/2, corr
	matched = DataFrame(peakNum1 = pkl1[!,"peakNum"][mat[:,1]],
						peakNum2 = pkl2[!,"peakNum"][mat[:,2]],
						tr1 = pkl1[!,"t_avg"][mat[:,1]],
						tr2 = pkl2[!,"t_avg"][mat[:,2]],
						spec2 = pkl2[!,"Spectrum"][mat[:,2]],
						correlation = vec(mat_cor))
	#Add unmatched peaks
	if size(pkl1,1) > size(mat,1)
		#Add unmatched peaks
		toAdd = findall(vec(all(any.(pkl1[!,"peakNum"] .== matched[!,"peakNum1"]') .== 0, dims = 2).==1))
		for p in toAdd
			matched = push!(matched, [pkl1[!,"peakNum"][p] false pkl1[!,"t_avg"][p] NaN "" NaN])
		end
	end
	if size(pkl2,1) > size(mat,1)
		#Add unmatched peaks
		toAdd = findall(vec(all(any.(pkl2[!,"peakNum"] .== matched[!,"peakNum2"]') .== 0, dims = 2).==1))
		for p in toAdd
			matched = push!(matched, [false pkl2[!,"peakNum"][p] NaN pkl2[!,"t_avg"][p] pkl2[!,"Spectrum"][p] NaN])
		end
	end

	return matched
end





function correlationMatching(spec1,spec2;kwargs...)
	#Calculate correlations
	if all(size(spec1) .== size(spec2)) && all(spec1 .== spec2)
		corrs = Correlations(spec1,spec2)
		#set impossible matches to 0
		for i = 1:size(spec1,1)
			corrs[i:end,i] .= 0
			corrs[findall(kwargs[:imp_matches][i,2].==kwargs[:imp_matches][:,2]),i] .= 0
			corrs[i,findall(kwargs[:imp_matches][i,2].==kwargs[:imp_matches][:,2])] .= 0
		end
	else
		corrs = Correlations(spec1[2:end,:],spec2[2:end,:])
	end

	#Match from high to low
	corrs_temp = copy(corrs)
	mat = zeros(minimum(size(corrs)),2)
	mat_cor = zeros(minimum(size(corrs)),1)
	inds = []
	num = 1
	while any(sum(mat,dims=2) .== 0) && any(corrs_temp .>= cor_thresh)
		#find highest correlation
		~,ind = findmax(corrs_temp)
		mat[num,1] = ind[1]
		mat[num,2] = ind[2]
		mat_cor[num,1] = corrs_temp[ind]
		if all(size(spec1) .== size(spec2)) && all(spec1 .== spec2)
			inf1 = kwargs[:imp_matches][ind[1],:]
			inf2 = kwargs[:imp_matches][ind[2],:]
			#if inf1 and inf2 would be merged
			corrs_temp[findall(inf2[2].==kwargs[:imp_matches][:,2]),ind[1]] .= -Inf
			corrs_temp[ind[1],findall(inf2[2].==kwargs[:imp_matches][:,2])] .= -Inf
			corrs_temp[findall(inf1[2].==kwargs[:imp_matches][:,2]),ind[2]] .= -Inf
			corrs_temp[ind[2],findall(inf1[2].==kwargs[:imp_matches][:,2])] .= -Inf
			corrs_temp[ind[1],ind[2]] = -Inf
		else
			corrs_temp[ind[1],:] .= -Inf
			corrs_temp[:,ind[2]] .= -Inf
		end
		num += 1
	end
	mat_cor = mat_cor[findall(vec(sum(mat, dims =2)) .!= 0),:]
	mat = Int.(mat[findall(vec(sum(mat, dims =2)) .!= 0),:])
	return mat, mat_cor
end



function mergePeakLists(file, pk_num, pk_tr, pk_spec, matched)
	for m = 1:size(matched,1)
		if isnan(matched[!,"tr2"][m])
			continue
		elseif isnan(matched[!,"tr1"][m])
			#add column for unmatched case
			pk_num = push!(pk_num,(([NaN.*zeros(1,size(pk_num,2)-1) matched[!,"peakNum2"][m]])))
			pk_tr = push!(pk_tr,([NaN.*zeros(1,size(pk_tr,2)-1) matched[!,"tr2"][m]]))
			pk_spec = push!(pk_spec,([fill("",(1,size(pk_spec,2)-1)) matched[!,"spec2"][m]]))
		else
			#save matched information for new file
			ind = findfirst(pk_num[:,1] .== matched[!,"peakNum1"][m])
			pk_num[!,file][ind] = matched[!,"peakNum2"][m]
			pk_tr[!,file][ind] = matched[!,"tr2"][m]
			pk_spec[!,file][ind] = matched[!,"spec2"][m]
		end
	end
	return pk_num, pk_tr, pk_spec
end





function backwardsMatching(pk_num, pk_tr, pk_spec)
	ind = DataFrame(row = [], file = [])
	pk_temp = Matrix(pk_num)

	#extract unmatched peaks
	for p = 1:size(pk_num,1)
		if isnan(pk_num[p,1])
			ip = findall(isnan.(pk_temp[p,:]) .==0)
			ind = push!(ind,(p,ip[1]))
		end
	end
	if isempty(ind)
		return pk_num, pk_tr, pk_spec
	end

	#obtains corresponding spectra
	pk_temp = Matrix(pk_spec)
	spec  = zeros(size(ind,1),length(parse.(Float64,split(pk_temp[ind[1,1],ind[1,2]][2:end-1],", "))))
	for i = 1:size(ind,1)
		spec[i,:] = parse.(Float64,split(pk_temp[ind[i,1],ind[i,2]][2:end-1],", "))
	end

	#perform matching
	mat, mat_cor = correlationMatching(spec,spec;imp_matches = ind)

	#merge matching results
	for m = 1:size(mat,1)
		if m != 1 && any(mat[1:m-1,2] .== mat[m,2])
			continue
		end
		t1 = ind[mat[m,1],:]
		t2 = ind[mat[m,2],:]
		#add t2 to t1
		pk_num[t1[1],t2[2]] = pk_num[t2[1],t2[2]]
		pk_num[t2[1],t2[2]]  = NaN
		pk_tr[t1[1],t2[2]] = pk_tr[t2[1],t2[2]]
		pk_tr[t2[1],t2[2]] = NaN
		pk_spec[t1[1],t2[2]] = pk_spec[t2[1],t2[2]]
		pk_spec[t2[1],t2[2]] = ""
		#adjust ind info for t2
		ind[mat[m,2],1] = ind[mat[m,1],1]
	end
	#delete rows without info
	stay = findall(vec(any(isnan.(Matrix(pk_num)) .== 0,dims = 2)))
	pk_num_out = pk_num[stay,:]
	pk_tr_out = pk_tr[stay,:]
	pk_spec_out = pk_spec[stay,:]

	return pk_num_out, pk_tr_out, pk_spec_out
end















## General functions
function spectraConvertion(pkl)
	#set up spec variable with lambda range in first row
	spec = zeros(size(pkl,1)+1,Int((pkl[!,"Max_wavelength"][1] - pkl[!,"Min_wavelength"][1])/pkl[!,"Step_size"][1])+1)
	spec[1,:] = pkl[!,"Min_wavelength"][1]:pkl[!,"Step_size"][1]:pkl[!,"Max_wavelength"][1]

	for s = 1:size(pkl,1)
		#convert spectrum from string to array
		extract = parse.(Float64,split(pkl[!,"Spectrum"][s][2:end-1],", "))
		#save
		spec[s+1,:] = extract
	end

	return spec
end


function Correlations(spec1,spec2)
	corrs = zeros(size(spec1,1),size(spec2,1))
	for s1 = 1:size(spec1,1)
		for s2 = 1:size(spec2,1)
			corrs[s1,s2] = cor(spec1[s1,:],spec2[s2,:])^2
		end
	end

	return corrs
end


function plotMatches(matched, pkl1, pkl2, path, file1, file2)
	println(path*"figures")
	if ~isdir(path*"figures")
		mkdir(path*"figures")
	end
	savepath = path*"figures\\"*file1[1:findlast('.',file1)-1]*" "*file2[1:findlast('.',file2)-1]*"\\"
	if ~isdir(savepath)
		mkdir(savepath)
	elseif size(readdir(savepath))[1] != 0
		#clear the folder
		rm.(savepath.*readdir(savepath))
	end

	#plot all matches + add corr to legend
	#Extract spectra from peak lists
	spec1 = spectraConvertion(pkl1)
	spec2 = spectraConvertion(pkl2)
	for m = 1:size(matched,1)
		if matched[!,"peakNum1"][m] .== 0 || matched[!,"peakNum2"][m] .== 0
			continue
		end
		sp1= matched[!,"peakNum1"][m]
		sp2= matched[!,"peakNum2"][m]
		cor = matched[!,"correlation"][m]
		plot(spec1[1,:],spec1[sp1+1,:])
		plot!(spec2[1,:],spec2[sp2+1,:])
		#Save plots
		savefig(savepath*"$sp1 with $sp2 corr = $cor.png")
	end

end




## testing section

#Batch
path = "C:\\Users\\dherwer\\OneDrive - UvA\\UV-Vis\\Datafiles\\Export3d_20 min\\"
type = "simpleCorr"
cor_thresh = 0.9
DADpeakTrackingBatch(path,type,cor_thresh)

# filename1 = "030-P2-D1-Mix 1.1_peak list.CSV"
# filename2 = "033-P2-D1-Mix 1.2_peak list.CSV"
# filename2 = "002-P2-D1-Mix 1.1_peak list.CSV"
# path1 = "C:\\Users\\dherwer\\OneDrive - UvA\\UV-Vis\\Datafiles\\Export3d_20 min"
# path2 = path1
# path2 = "C:\\Users\\dherwer\\OneDrive - UvA\\UV-Vis\\Datafiles\\Export3d_5 min"
#
# toMatch = readdir(path1)
#
#
# matched = DADpeakTracking(filename1,path1,filename2,path2,"simpleCorr")


## Tijmen
# filename1 = "012-P1-E1-Dye 1-25 MegaMix_peak list.CSV"
# filename2 = "004-P1-E1-Dye 1-25 MegaMix_peak list.CSV"
# path1 = "C:\\Users\\dherwer\\OneDrive - UvA\\UV-Vis\\Datafiles\\Tijmen\\9 min\\"
# path2 = "C:\\Users\\dherwer\\OneDrive - UvA\\UV-Vis\\Datafiles\\Tijmen\\3 min\\"
#
# toMatch = readdir(path1)
#
#
# matched = DADpeakTracking(filename1,path1,filename2,path2,"simpleCorr")
