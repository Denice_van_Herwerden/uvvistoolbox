using LinearAlgebra, CSV, DataFrames, Statistics, Plots
include("DAD_Import.jl")



function PeakSelection(path,bgfile)
	files = readdir(path)
	tr_bg, signal_bg = importDAD(bgfile, path)
	comp_pkl = []
	for f = 1:length(files)
		if isfile(path*files[f]) && !contains(files[f],"_peak list") && !contains(files[f],"Mix") && (files[f] .!= bgfile)
			#load file
			println("Processing file: "*files[f])
			trw, signal, lambda = importDAD(files[f],path)
			if size(signal) == size(signal_bg)
				signal = signal .- signal_bg
			else
				println("Background correction skipped due to mismatching size")
			end
			#laod peak list
			if isfile(path*files[f][1:findlast('.',files[f])-1]*"_peak list.csv")
				peak_list = CSV.read(path*files[f][1:findlast('.',files[f])-1]*"_peak list.csv",DataFrame)
			else
				println(path*files[f][1:findlast('.',files[f])-1]*"_peak list.csv is not a valid peak list file")
				return
			end

			#Calc dot prodct with
			comp_tr = peak_list[!,"t_avg"][Int(findmax(peak_list[!,"maxInt"])[2])]
			ind = findfirst(trw .>= comp_tr)
			res = zeros(size(signal,1),1)
			dot_ref = sum((signal[ind,:]./maximum(signal[ind,:])).^2)
			for i = 1:size(signal,1)
				dot_direct = sum(signal[i,:]./maximum(signal[i,:]).*signal[ind,:]./maximum(signal[ind,:]))
				res[i,1] = round(1-abs(dot_ref-dot_direct),digits=2)
			end

			#save the plot
			plot(trw,res)
			plot!(trw, (25 .*(sum(signal, dims=2)./maximum(sum(signal, dims=2)))).-25,legend=false)
			xlabel!("Time (min)")
			ylabel!("Normalized dot product")
			ylims!(-100,2)
			savefig(path*"figures\\"*files[f][1:findlast('.',files[f])-1]*"\\"*files[f][1:end-4]*"_dot.png")

			#Add peak to full peak list
			peak_list[!,"normDot"] = ones(size(peak_list,1)).*NaN
			peak_list[!,"file"] = string.(ones(size(peak_list,1)))
			for p = 1:size(peak_list,1)
				ind_peak = findfirst(trw .>= peak_list[!,"t_avg"][p])
				peak_list[!,"normDot"][p] = res[ind_peak,1]
				peak_list[!,"file"][p] = files[f]
			end
			if isempty(comp_pkl)
				comp_pkl = peak_list
			else
				comp_pkl = append!(comp_pkl,peak_list)
			end
		end
	end
	CSV.write(path*"comprehensive_peak list.csv",comp_pkl)
	return comp_pkl
end





## Testing area

bgfile = "029-P2-B11-Blank.CSV"
path = "C:\\Users\\dherwer\\OneDrive - UvA\\UV-Vis\\Datafiles\\Export3d_20 min\\figures_minInt 05\\"
comp_pkl = PeakSelection(path,bgfile)

bgfile = "031-P2-B11-Blank.CSV"
path = "C:\\Users\\dherwer\\OneDrive - UvA\\UV-Vis\\Datafiles\\Export3d_10 min\\"
comp_pkl = PeakSelection(path,bgfile)

bgfile = "004-P2-B11-Blank.CSV"
path = "C:\\Users\\dherwer\\OneDrive - UvA\\UV-Vis\\Datafiles\\Export3d_5 min\\"
comp_pkl = PeakSelection(path,bgfile)

bgfile = "035-P2-B11-Blank.CSV"
path = "C:\\Users\\dherwer\\OneDrive - UvA\\UV-Vis\\Datafiles\\Export3d_2 5 min\\"
comp_pkl = PeakSelection(path,bgfile)

bgfile = "035-P2-B11-Blank.CSV"
path = "C:\\Users\\dherwer\\OneDrive - UvA\\UV-Vis\\Datafiles\\Export3d_1 min\\"
comp_pkl = PeakSelection(path,bgfile)


tr_bg, signal_bg = importDAD(bgfile, path)

tr, signal, wv = importDAD(filename,path)

ind = findfirst(tr .>= 10.08686667)
corr = zeros(size(signal,1),1)
for i = 1:size(signal,1)
	corr[i,1] = dot(signal[i,:]./maximum(signal[i,:]),signal[ind,:]./maximum(signal[ind,:]))
end



for i = 1:1488
	if any(des1_A.==i) && any(des1_D .==i)
	elseif any(des1_A.==i) || any(des1_D .==i)
		println(i)
	end
end
