using LinearAlgebra, CSV, DataFrames, Statistics, Plots
include("DAD_Import.jl")
include("DAD_Peak_Detection.jl")
include("DAD_Deconvolution.jl")



function SensitivityCalculations(path,bgfile,time_window,minInt,deltaTime,minLambdaCount,type)
	#minInt, deltaTime, minWV are vectors containing the values for which the peak detection should be performed


	#Remove peak lists
	#files = readdir(path)
	#rm.(path.*"\\".*files[contains.(files,"Mix") .& contains.(files,"_peak list")])
	files = readdir(path)

	filesToProc = files[contains.(files,"Mix")]

	for mI in minInt
		for dT in deltaTime
			for mL in minLambdaCount
				println("Processing: mI $mI dT $dT mWV $mL")
				dest = path*"mI $mI dT $dT mWV $mL"
				#Only else can be kept if full workflow is complete
				if isdir(dest)
				# 	continue
				# 	cp.(path.*"\\".*filesToProc,dest.*"\\".*filesToProc,force = true)
				# 	cp(path*"\\"*bgfile,dest*"\\"*bgfile,force = true)
				# 	#DADPeakDetectionBatch(dest, mI, dT, mL, type; filename_bg = bgfile, time_window = time_window, filenames = filesToProc)
				# 	DADPeakDetectionBatch(dest, mI, dT, mL, type; MovingAvg = true, time_window = time_window, filenames = filesToProc)#, figures = false)
				# 	DADdeconvolutionBatch(dest,"direct";filename_bg = bgfile)
				# 	rm.(dest.*"\\".*filesToProc)
				# 	rm.(dest*"\\"*bgfile)
				else
					mkdir(dest)
					cp.(path.*filesToProc,dest.*"\\".*filesToProc,force = true)
					cp(path*bgfile,dest*"\\"*bgfile,force = true)
					DADPeakDetectionBatch(dest, mI, dT, mL, type; MovingAvg = true, time_window = time_window, filenames = filesToProc)
					#DADPeakDetectionBatch(dest, mI, dT, mL, type; filename_bg = bgfile, time_window = time_window, filenames = filesToProc)
					DADdeconvolutionBatch(dest,"direct";filename_bg = bgfile)
					rm.(dest.*"\\".*filesToProc)
					rm.(dest*"\\"*bgfile)
				end


			end
		end
	end
end

function SensitivityEvaluation(path,reference, timeThresh, corrThresh)

	#obtain folders
	folders = readdir(path)
	folders = folders[contains.(folders," mWV ")]
	#Get reference file + filter out thresh
	ref = CSV.read(path*reference,DataFrame)
	ref = ref[ref[!,"Peak"] .== 1,:]

	#structure with filename, mI, dT, mL, FP,FN TP
	res = DataFrame(minInt = ones(length(folders)*15), deltaTime = NaN, minLambdaCount = NaN, file = "", TP = NaN, FP = NaN, FN = NaN, Comment = "")
	rates = DataFrame(Folder = folders, minInt = NaN, deltaTime = NaN, minLambdaCount = NaN, FPrate =NaN, FNrate = NaN)
	detRes = zeros(size(ref,1),length(folders)*15)

	cnt = 1
	for j = 1:length(folders)
		#each dir starting with mI
		println(folders[j])
		info = split(folders[j]," ")
		files = readdir(path*folders[j])
		for f = 1:length(files)
			#each file
			if !isdir(path*folders[j]*"\\"*files[f])
				println(files[f])
				#Add info to dataframe
				res[!,"minInt"][cnt] = parse(Float64,info[2])
				res[!,"deltaTime"][cnt] = parse(Float64,info[4])
				res[!,"minLambdaCount"][cnt] = parse(Float64,info[6])
				res[!,"file"][cnt] = files[f]
				#load peak list
				pkl = CSV.read(path*folders[j]*"\\"*files[f],DataFrame)
				#Calculate num FP FN TP with ref
				dTime = abs.(ref[!,"t_avg"] .- pkl[!,"t_avg"]')
				TP = 0
				#Delete old figs
				figs = readdir(path*folders[j]*"\\figures\\"*files[f][1:findlast('_',files[f])-1])
				rm.((path*folders[j]*"\\figures\\"*files[f][1:findlast('_',files[f])-1]).*"\\".*figs[contains.(figs,"X ref ")])
				#tr and spec matching
				while any(dTime .< timeThresh)
					ind = findmin(dTime)[2]
					sref = parse.(Float64,split(ref[!,"Spectrum"][ind[1]][2:end-1], ","))
					spkl = parse.(Float64,split(pkl[!,"Spectrum"][ind[2]][2:end-1], ","))
					cr = cor(sref./maximum(sref),spkl./maximum(spkl)).^2
					dt = dot(sref./maximum(sref),spkl./maximum(spkl))/dot(sref./maximum(sref),sref./maximum(sref))
					t1 = round(ref[!,"t_avg"][ind[1]],digits = 3)
					t2 = round(pkl[!,"t_avg"][ind[2]],digits = 3)
					#Fig 1: spec + corr

					if cr > corrThresh
						#Save data
						TP += 1
						dTime[ind[1],:] .= Inf
						dTime[:,ind[2]] .= Inf
						refnum = ind[1]
						samnum = ind[2]
						detRes[ind[1],cnt] = 1
						plot(sref./maximum(sref),label = "Ref $t1 corr = $cr")
						plot!(spkl./maximum(spkl),label = "Sample $t2 dot = $dt")
						savefig(path*folders[j]*"\\figures\\"*files[f][1:findlast('_',files[f])-1]*"\\X ref $refnum sample $samnum.png")
					else
						#Not an accepted match
						dTime[ind] = Inf
					end
				end

				res[!,"TP"][cnt] = TP
				res[!,"FP"][cnt] = size(dTime)[2]-TP
				res[!,"FN"][cnt] = size(dTime)[1]-TP
				comments = ref[!,"Info"][vec(all(dTime.==Inf,dims=2))]
				comments = comments[comments.!="\"\""]
				res[!,"Comment"][cnt] = string(comments)

				#Only for Mi = 1
				if contains(folders[j], "mI 1 ")
					trw, chrom = importDAD(files[f][1:end-14]*".csv",path)
					dots = zeros(size(chrom,1),sum(any(dTime .< Inf, dims=2)))
					#Not found compounds?
					num = 1
					for i = 1:size(dTime,1)
						if any(dTime[i,:] .< Inf)
							sFN = parse.(Float64,split(ref[!,"Spectrum"][i][2:end-1], ","))
							for x = 1:size(chrom,1)
								dots[x,num] = dot(sFN, chrom[x,:])/sqrt(sum(sFN.^2)*sum(chrom[x,:].^2))
							end
							dots[:,num] = dots[:,num]./dot(sFN./maximum(sFN),sFN./maximum(sFN))
							#plot with indication of time reference
							plot(trw,dots[:,num])
							scatter!([ref[!,"t_avg"][i]],[0], seriesType = :scatter)
							#ylims!(-1,1)
							savefig(path*folders[j]*"\\figures\\"*files[f][1:findlast('_',files[f])-1]*"\\Dot peak $i.png")
							num += 1
						end
					end
					CSV.write(path*folders[j]*"\\figures\\"*files[f][1:findlast('_',files[f])-1]*"\\Dots.csv")
				else
					return
				end

				#Next
				cnt += 1
			end
		end
		#calculate rates										######################
		rates[!,"minInt"][j] = parse(Float64,info[2])
		rates[!,"deltaTime"][j] = parse(Float64,info[4])
		rates[!,"minLambdaCount"][j] = parse(Float64,info[6])
		TPs = sum(res[!,"TP"][(res[!,"minInt"] .== res[!,"minInt"][cnt-1]) .& (res[!,"deltaTime"] .== res[!,"deltaTime"][cnt-1]) .& (res[!,"minLambdaCount"] .== res[!,"minLambdaCount"][cnt-1])])
		FPs = sum(res[!,"FP"][(res[!,"minInt"] .== res[!,"minInt"][cnt-1]) .& (res[!,"deltaTime"] .== res[!,"deltaTime"][cnt-1]) .& (res[!,"minLambdaCount"] .== res[!,"minLambdaCount"][cnt-1])])
		FNs = sum(res[!,"FN"][(res[!,"minInt"] .== res[!,"minInt"][cnt-1]) .& (res[!,"deltaTime"] .== res[!,"deltaTime"][cnt-1]) .& (res[!,"minLambdaCount"] .== res[!,"minLambdaCount"][cnt-1])])
		rates[!,"FPrate"][j] = FPs/(TPs+FPs)*100
		rates[!,"FNrate"][j] = FNs/(TPs+FNs)*100
	end

	CSV.write(path*"Induvidual false detection.csv",res)
	CSV.write(path*"False detection rates.csv",rates)

	return detRes
end


## Testing area
minInt = [1 2 5 8 10]#[1 2 5 8 10]		#minimum required intensity of the peak
deltaTime = [0.001 0.005 0.01 0.025]	#Time maximum time difference allowed for matching peak tops between different wavelengths
minWV = [2 5 10 15] 				#Minimum wavelength range required for a group of peak tops to be accepted as a part of an spectrum (in nm)
type = "MWPD"					#Peak detection type

time_window = 21
bgfile = "029-P2-B11-Blank.CSV"
path = "C:\\Users\\dherwer\\OneDrive - UvA\\UV-Vis\\Datafiles\\Export3d_20 min\\"
SensitivityCalculations(path,bgfile,time_window,minInt,deltaTime,minWV,type)
timeThresh = 0.1
corrThresh = 0.5
chrom = importDAD("030-P2-D1-Mix 1.1.CSV",path)
detRes = SensitivityEvaluation(path,"comprehensive_peak list.csv", timeThresh, corrThresh)

#Plot
rates = CSV.read(path*"\\Sens 1\\False detection rates.csv",DataFrame)
rates = rates[rates[!,"minLambdaCount"].==5,:]
rates = rates[rates[!,"deltaTime"].==0.01,:]
rates2 = CSV.read(path*"\\Sens New +bg rm\\False detection rates.csv",DataFrame)
rates3 = CSV.read(path*"\\False detection rates.csv",DataFrame)

plot(rates[!,"minInt"],rates[!,"FPrate"],seriestype = scatter,label = "Old FD")
plot!(rates[!,"minInt"],rates[!,"FNrate"],seriestype = scatter,label = "Old FN")
plot!(rates2[!,"minInt"],rates2[!,"FPrate"],seriestype = scatter,label = "New FD")
plot!(rates2[!,"minInt"],rates2[!,"FNrate"],seriestype = scatter,label = "New FN")
plot!(rates3[!,"minInt"],rates3[!,"FPrate"],seriestype = scatter,label = "New -bgrm FD")
plot!(rates3[!,"minInt"],rates3[!,"FNrate"],seriestype = scatter,label = "New -bgrm FN")
title!("deltaTime = 0.01, minimumLambdaCount = 5")
xlabel!("minimum Intensity")
ylabel!("Rate (%)")
ylims!(0,100)

plot(rates[!,"deltaTime"],rates[!,"FPrate"],seriestype = scatter,label = "dT FP")
plot(rates[!,"deltaTime"],rates[!,"FNrate"],seriestype = scatter,label = "dT FN")
plot!(rates3[!,"deltaTime"],rates3[!,"FPrate"],seriestype = scatter,label = "dT FP")
plot!(rates3[!,"deltaTime"],rates3[!,"FNrate"],seriestype = scatter,label = "dT FN")

plot(rates[!,"minLambdaCount"],rates[!,"FPrate"],seriestype = scatter,label = "minIntFP")
plot(rates[!,"minLambdaCount"],rates[!,"FNrate"],seriestype = scatter,label = "minIntFN")
plot!(rates3[!,"minLambdaCount"],rates3[!,"FPrate"],seriestype = scatter,label = "minIntFP")
plot!(rates3[!,"minLambdaCount"],rates3[!,"FNrate"],seriestype = scatter,label = "minIntFN")



time_window = 11
bgfile = "031-P2-B11-Blank.CSV"
path = "C:\\Users\\dherwer\\OneDrive - UvA\\UV-Vis\\Datafiles\\Export3d_10 min"
comp_pkl = PeakSelection(path,bgfile)

time_window = 6
bgfile = "004-P2-B11-Blank.CSV"
path = "C:\\Users\\dherwer\\OneDrive - UvA\\UV-Vis\\Datafiles\\Export3d_5 min"
comp_pkl = PeakSelection(path,bgfile)

time_window = 3.49
bgfile = "035-P2-B11-Blank.CSV"
path = "C:\\Users\\dherwer\\OneDrive - UvA\\UV-Vis\\Datafiles\\Export3d_2 5 min"
comp_pkl = PeakSelection(path,bgfile)


time_window = 1.99
bgfile = "035-P2-B11-Blank.CSV"
path = "C:\\Users\\dherwer\\OneDrive - UvA\\UV-Vis\\Datafiles\\Export3d_1 min"
comp_pkl = PeakSelection(path,bgfile)


tr_bg, signal_bg = importDAD(bgfile, path)

tr, signal, wv = importDAD(filename,path)

ind = findfirst(tr .>= 10.08686667)
corr = zeros(size(signal,1),1)
for i = 1:size(signal,1)
	corr[i,1] = dot(signal[i,:]./maximum(signal[i,:]),signal[ind,:]./maximum(signal[ind,:]))
end



for i = 1:1488
	if any(des1_A.==i) && any(des1_D .==i)
	elseif any(des1_A.==i) || any(des1_D .==i)
		println(i)
	end
end
