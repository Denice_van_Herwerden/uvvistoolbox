module UVvisToolBox

#Julia packages
using CSV
using DataFrames
using DelimitedFiles
using StringEncodings
using Findpeaks
using Statistics
using LinearAlgebra
using SparseArrays
using Plots
using BSON


#Python packages
using Conda
try
	using PyCall
catch
	Pkg.build("PyCall")
	using PyCall
end
try
    pip = pyimport("pip")
catch
    get_pip = joinpath(dirname(@__FILE__), "get-pip.py")
    download("https://bootstrap.pypa.io/get-pip.py", get_pip)
    run(`$(PyCall.python) $get_pip --user`)
	pip = pyimport("pip")
end
pyPkg = ["pymcr" "lmfit" "bson"]
ctf = false
for p = 1:length(pyPkg)
	try
		pyimport(pyPkg[p])
	catch
		Conda.pip_interop(true)
		Conda.pip("install", pyPkg[p])
		ctf = true
	end
end
if ctf == true
	Pkg.build("PyCall")
	using PyCall
end



#Modules
include("DAD_Import.jl")
include("DAD_Peak_Detection.jl")
include("DAD_Deconvolution.jl")
include("DAD_Peak_Tracking.jl")


export importDAD,
	   DADPeakDetection, DADPeakDetectionBatch,
	   DADdeconvolution, DADdeconvolutionBatch,
	   DADPeakTracking

end
